package com.example.demoAppSampleA;

import java.time.Duration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class DemoAppSampleAApplication {

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		;
		return restTemplateBuilder.setConnectTimeout(Duration.ofSeconds(2)).build();
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoAppSampleAApplication.class, args);
	}

}
