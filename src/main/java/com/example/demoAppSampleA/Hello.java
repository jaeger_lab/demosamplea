package com.example.demoAppSampleA;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
class HelloWorld {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    PersonRepository person;

    @RequestMapping("/hello")
    public String hello() {
        return "Hello App A";
    }

    @RequestMapping("/chaining")
    public String chaining() {
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8080/helloB", String.class);
        return "Chaining : " + response.getBody();
    }

    @RequestMapping("/clientetimeout")
    public String clientetimeout() {
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8080/timeOut", String.class);
        return "Chaining : " + response.getBody();
    }

}